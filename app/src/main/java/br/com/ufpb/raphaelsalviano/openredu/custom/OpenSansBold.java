package br.com.ufpb.raphaelsalviano.openredu.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by rapha on 09/11/2016.
 */

public class OpenSansBold extends TextView {

    public OpenSansBold(Context context) {
        super(context);
        setTypeface(context);
    }

    public OpenSansBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(context);
    }

    public OpenSansBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public OpenSansBold(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setTypeface(context);
    }

    private void setTypeface (Context context){
        setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/bold.ttf"));
    }
}
