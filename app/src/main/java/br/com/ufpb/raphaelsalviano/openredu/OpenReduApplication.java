package br.com.ufpb.raphaelsalviano.openredu;

import android.app.Application;

/**
 * Created by rapha on 10/11/2016.
 */

public class OpenReduApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
