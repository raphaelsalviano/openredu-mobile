package br.com.ufpb.raphaelsalviano.openredu.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import br.com.ufpb.raphaelsalviano.openredu.R;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        createDrawer(savedInstanceState);

    }

    private void createDrawer(Bundle savedInstanceState) {
        AccountHeader accountHeader = new AccountHeaderBuilder().withActivity(this)
                .withCompactStyle(false)
                .withSavedInstance(savedInstanceState)
                .withHeaderBackground(R.color.md_blue_700)
                .addProfiles(
                        new ProfileDrawerItem().withIcon(R.drawable.img_profile)
                                .withName("John Tyree")
                                .withEmail("john_tyree@gmail.com")
                                .withTypeface(createTypeface())
                                .withIdentifier(0)
                )
                .build();

        new DrawerBuilder().withActivity(this)
                .withActionBarDrawerToggleAnimated(true)
                .withDrawerGravity(Gravity.START)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .withItemAnimator(new DefaultItemAnimator())
                .withSelectedItem(-1)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Meu mural")
                                .withIcon(R.drawable.ic_feed)
                                .withTag("mural"),
                        new PrimaryDrawerItem().withName("Mensagens e Contatos")
                                .withIcon(R.drawable.ic_message)
                                .withTag("mensagens"),
                        new PrimaryDrawerItem().withName("Ambientes")
                                .withIcon(R.drawable.ic_ambiente)
                                .withTag("ambientes")
                )
                .addDrawerItems(new DividerDrawerItem()
                )
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Configurações")
                                .withIcon(R.drawable.ic_settings)
                                .withTag("config"),
                        new PrimaryDrawerItem().withName("Sair")
                                .withIcon(R.drawable.ic_logout)
                                .withTag("sair")
                )
                .withOnDrawerItemClickListener(new DrawerListener())
                .build();
    }

    private Typeface createTypeface() {
        return Typeface.createFromAsset(getAssets(), "fonts/regular.ttf");
    }

    private class DrawerListener implements Drawer.OnDrawerItemClickListener {

        @Override
        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

            String TAG = drawerItem.getTag().toString();

            Intent intent = null;

            switch (TAG){
                case "mural":
                    return true;
                case "mensagens":
                    return true;
                case "ambientes":
                    return true;
                case "settings":
                    return true;
                case "sair":
                    return true;
            }

            return false;
        }
    }


}
